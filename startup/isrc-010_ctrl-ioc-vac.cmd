# @field VEVMC_DEVICENAME
# @type STRING
# VEVMC Device name, should be the same as the name in CCDB

# @field IPADDR
# @type STRING
# Device IP address (Ethernet2Serial converter)

# @field PORT
# @type INTEGER
# Comms port on Ethernet2Serial converter


requireSnippet(vac_ctrl_mks946_937b_ethernet.cmd, "DEVICENAME = $(VEVMC_DEVICENAME), IPADDR = $(IPADDR), PORT = $(PORT)")
